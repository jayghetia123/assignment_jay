//
//  Geners+CoreDataClass.swift
//  
//
//  Created by JayMac on 27/10/17.
//
//

import Foundation
import CoreData

@objc(Movies)
extension Geners  {
    
    class func fetchGenersType() -> ([Geners]){
        
        let entityDescription =
            NSEntityDescription.entity(forEntityName: "Geners",
                                       in: CoreDataManager.CoreDataManagerShared.persistentContainer.viewContext)
        
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = entityDescription
        
        do {
            let objects = try CoreDataManager.CoreDataManagerShared.persistentContainer.viewContext.fetch(request)
            
            if objects.count > 0 {
                return objects as! [Geners]
            }else {
                return [Geners]()
            }
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        return [Geners]()
    }
}
