//
//  Geners+CoreDataProperties.swift
//  
//
//  Created by JayMac on 27/10/17.
//
//

import Foundation
import CoreData

extension Geners {

    @NSManaged public var title: String?
    @NSManaged public var movies: Movies
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Geners> {
        return NSFetchRequest<Geners>(entityName: "Geners")
    }
}
