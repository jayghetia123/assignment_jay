//
//  Movies+CoreDataProperties.swift
//  
//
//  Created by JayMac on 27/10/17.
//
//

import Foundation
import CoreData

extension Movies {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movies> {
        return NSFetchRequest<Movies>(entityName: "Movies")
    }

    @NSManaged public var imageURL: String?
    @NSManaged public var rating: String?
    @NSManaged public var releaseYear: String?
    @NSManaged public var title: String?
    @NSManaged public var geners: Set<Geners>

}

// MARK: Generated accessors for geners
extension Movies {

    @objc(addGenersObject:)
    @NSManaged public func addToGeners(_ value: Geners)

    @objc(removeGenersObject:)
    @NSManaged public func removeFromGeners(_ value: Geners)

    @objc(addGeners:)
    @NSManaged public func addToGeners(_ values: NSSet)

    @objc(removeGeners:)
    @NSManaged public func removeFromGeners(_ values: NSSet)

}
