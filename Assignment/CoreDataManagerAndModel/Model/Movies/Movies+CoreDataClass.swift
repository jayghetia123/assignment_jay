//
//  Movies+CoreDataClass.swift
//  
//
//  Created by JayMac on 27/10/17.
//
//

import Foundation
import CoreData

@objc(Movies)
extension Movies  {

    class func isMovieExist(strMovieName: String) -> (Bool){

        let entityDescription =
            NSEntityDescription.entity(forEntityName: "Movies",
                                       in: CoreDataManager.CoreDataManagerShared.persistentContainer.viewContext)

        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = entityDescription

        var pred = NSPredicate()
        pred = NSPredicate(format: "title = %@", strMovieName)
        request.predicate = pred
        do {
            let objects = try CoreDataManager.CoreDataManagerShared.persistentContainer.viewContext.fetch(request)

            if objects.count > 0 {
                return true
            }else {
                return false
            }

        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        return true
    }
    
    class func insertMoviesInDatabase(arrDictMovies : [[String : Any]]) {
        
        print(CoreDataManager.CoreDataManagerShared.applicationDocumentsDirectory)
        
        for dictMovie in arrDictMovies {
            
            if !Movies.isMovieExist(strMovieName: getStringFromAnyObject(dictMovie["title"])) {
                
                let movie = Movies(context:CoreDataManager.CoreDataManagerShared.persistentContainer.viewContext)
                
                movie.title = getStringFromAnyObject(dictMovie["title"])
                movie.rating = getStringFromAnyObject(dictMovie["rating"])
                movie.releaseYear = getStringFromAnyObject(dictMovie["releaseYear"])
                movie.imageURL = getStringFromAnyObject(dictMovie["image"])
                
                
                let arrStrGeners = Set(dictMovie["genre"] as! [String])
                
                let arrObjGeners = arrStrGeners.map { (strGener : String) -> Geners in
                
                    let geners = Geners(context:CoreDataManager.CoreDataManagerShared.persistentContainer.viewContext)
                                        
                    geners.title =  strGener
                    return geners
                }
                
                movie.geners = Set(arrObjGeners)
                
            }
        }
        CoreDataManager.CoreDataManagerShared.saveContext()
    }
    
    class func fetchAllMoviesList() -> ([Movies]){
        
        let entityDescription =
            NSEntityDescription.entity(forEntityName: "Movies",
                                       in: CoreDataManager.CoreDataManagerShared.persistentContainer.viewContext)
        
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = entityDescription
        
        do {
            let objects = try CoreDataManager.CoreDataManagerShared.persistentContainer.viewContext.fetch(request)
            
            if objects.count > 0 {
                return objects as! [Movies]
            }else {
                return [Movies]()
            }
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        return [Movies]()
    }
}


