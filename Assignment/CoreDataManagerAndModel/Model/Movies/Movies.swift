//
//  Moives.swift
//  Assignment
//
//  Created by JayMac on 26/10/17.
//  Copyright © 2017 JayMac. All rights reserved.
//

import UIKit
import Foundation
import CoreData

@objc(Movies)
public class Movies: NSManagedObject {
    
    func getMoviesList(responseTo:@escaping ((Response) -> Void)) {
        
        NetworkManager.BaseRequestSharedInstance.GETRequset(showLoader: true, url: WebService.getMovies, success: { (response:AnyObject!) in
            
            let objResponse : Response = Response.init(response: response)
            print(objResponse)
            responseTo(objResponse)
            
            //Insert movie data in database
            Movies.insertMoviesInDatabase(arrDictMovies: objResponse.data as! [[String : Any]])
            
        }) { (responseFaile:AnyObject!) in
            
            let objResponse : Response = Response.init(response: responseFaile)
            print(objResponse)
            responseTo(objResponse)
        }
    }
}

