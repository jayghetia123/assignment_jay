//
//  MovieListingVC.swift
//  Assignment
//
//  Created by JayMac on 27/10/17.
//  Copyright © 2017 JayMac. All rights reserved.
//

import UIKit

class MovieListingVC: UIViewController {

    @IBOutlet weak var cvMovieList: UICollectionView!
    var movieListingVM : MovieListingVM!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        movieListingVM = MovieListingVM.init(collectionView: cvMovieList, reloadCollectionViewCallback: reloadCollectionViewData)
    }

    @IBAction func btnNavFilterClicked(_ sender: UIBarButtonItem) {
        
        movieListingVM.showFilterOption(viewController: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //call by MovingListingVM when new data featched
    func reloadCollectionViewData(){
        
        cvMovieList.reloadData()
    }
}

extension MovieListingVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return movieListingVM.sizeForCollectionView()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return movieListingVM.numberOfSectionInCollectionView()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieListingVM.numberOfItemInSection(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return movieListingVM.setDataInCollectionView(indexPath: indexPath, collectionView : collectionView)
    }
    
}
