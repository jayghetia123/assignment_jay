//
//  MovieCell.swift
//  Assignment
//
//  Created by JayMac on 27/10/17.
//  Copyright © 2017 JayMac. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCell: UICollectionViewCell {

    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupData(rating : Float?, releaseDate: String?, strImage: String?){
        
        lblRating.text = String(format: "%.01f", rating ?? 0.0)
        lblReleaseDate.text = releaseDate ?? ""
        
        guard let unwrapeImage = strImage else {
            return
        }
        
        imgMovie.sd_setImage(with: URL(string: unwrapeImage), placeholderImage: UIImage(named:  "PlaceHolderMovie"))
        
    }

}
