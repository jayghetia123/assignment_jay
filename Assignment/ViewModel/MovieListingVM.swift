//
//  MovieListingVM.swift
//  Assignment
//
//  Created by JayMac on 27/10/17.
//  Copyright © 2017 JayMac. All rights reserved.
//

import Foundation
import UIKit

class MovieListingVM: NSObject {
    
    var reloadCollectionViewCallback : (()->())!
    let intNumberOfSectionInCollectionView = 1
    var arrMovie = [Movies]()
    var arrGenersType  = Set<String>()
    var strSelectedGenersType  = ""
    var refresher:UIRefreshControl!
    
    init(collectionView: UICollectionView, reloadCollectionViewCallback : @escaping (()->())) {
        
        super.init()
        
        collectionView.register(UINib(nibName: "MovieCell", bundle: nil), forCellWithReuseIdentifier: "MovieCell")
        self.reloadCollectionViewCallback = reloadCollectionViewCallback
        
        // Add pull to referesh in collection view
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = UIColor.darkGray
        self.refresher.addTarget(self, action: #selector(pullDownToReferesh), for: .valueChanged)
        collectionView.addSubview(refresher)
        
        // get data from database
        self.fetchAllMovieListFromDatabase()
        
        // get latest data from server
        getMovieListing()
    }

    // MARK:- Referesh Controller methods
    
    @objc func pullDownToReferesh() {
        
        getMovieListing()
    }
    
    func stopRefresher() {
        self.refresher.endRefreshing()
    }
    
    //MARK:- get data
    func getMovieListing() {
        
        let objMovie = Movies()
        
        objMovie.getMoviesList { (objResponse) in

            if(objResponse.isSuccess == ResponseStatus.False) {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    showAlert(objResponse.strMessage!)
                }
            }
            self.fetchAllMovieListFromDatabase()
        }
    }
    
    func fetchAllMovieListFromDatabase() {
        
        // Fetch data from database
        if strSelectedGenersType == "" {
            self.arrMovie = Movies.fetchAllMoviesList()
        } else {
            filterDataUsingGenersType(strType: strSelectedGenersType)
        }
        
        DispatchQueue.main.async {
            self.stopRefresher()
            self.reloadCollectionViewCallback()
        }
    }
    
    //MARK:- Methods for collection view
    
    func numberOfItemInSection(section : Int) -> Int {
        return arrMovie.count
    }
    
    func numberOfSectionInCollectionView() -> Int {
        return intNumberOfSectionInCollectionView
    }
    
    func sizeForCollectionView() -> CGSize {
        return CGSize.init(width: (SCREEN_WIDTH/2) - 15, height: (SCREEN_WIDTH/2) - 15)
    }
    
    func setDataInCollectionView(indexPath : IndexPath, collectionView : UICollectionView) -> UICollectionViewCell {
        
        let objMovie = arrMovie[indexPath.row]
        
        let cell: MovieCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCell
        
        cell.setupData(rating: Float(getStringFromAnyObject(objMovie.rating)), releaseDate: objMovie.releaseYear, strImage: objMovie.imageURL)
        
        return cell
    }
    
    //MARK:- Filter functionality
    
    func showFilterOption(viewController : UIViewController) {
        
        // To get unique geners list
        getGenersType()
        
        let alert:UIAlertController = UIAlertController(title: "Filter By Genere",
                                                        message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let actionNone = UIAlertAction(title: "None", style: UIAlertActionStyle.default) { action in
            
            // Fetch data from database
            self.strSelectedGenersType = ""
            self.fetchAllMovieListFromDatabase()
        }
        alert.addAction(actionNone)
        
        for strGenersType in arrGenersType {
        
            let actionType = UIAlertAction(title: strGenersType, style: UIAlertActionStyle.default) { action in
                
                self.strSelectedGenersType = action.title!
                self.filterDataUsingGenersType(strType: action.title!)
                
            }
            
            alert.addAction(actionType)
        }

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func getGenersType() {
        
        let geners = Geners.fetchGenersType()
        let arrStrGenersType = geners.map { (objGeners: Geners) -> String in
            return objGeners.title!
        }
        arrGenersType = Set(arrStrGenersType)
    }
    
    func filterDataUsingGenersType(strType : String) {
        
        let arrFilterData = Movies.fetchAllMoviesList().filter { (objMovie : Movies) -> Bool in
            
            let strTitles = objMovie.geners.map({ (gener) -> String in
                return gener.title!
            })
            if strTitles.contains(strType)
            {
                return true
            }
            return false
        }
        
        arrMovie = arrFilterData
        DispatchQueue.main.async {
            self.reloadCollectionViewCallback()
        }
    }
    
}
