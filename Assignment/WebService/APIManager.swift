//
//  APIManager.swift
//  Assignment
//
//  Created by JayMac on 27/10/17.
//  Copyright © 2017 JayMac. All rights reserved.
//

import UIKit
import MBProgressHUD

class NetworkManager: NSObject {
    
    static let BaseRequestSharedInstance = NetworkManager()
    
    //MARK: - WS CALLS Methods
    
    func GETRequset(showLoader: Bool, url:String, success:@escaping (AnyObject!) -> Void, failed:@escaping (AnyObject!) -> Void) {
        
        self.startProgressLoading(isStart: showLoader)
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                print("O/P error=\(error.debugDescription)")
                self.stopProgressLoading()
                failed(error.debugDescription as AnyObject!)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("O/P statusCode should be 200, but is \(httpStatus.statusCode)")
                print("O/P response = \(String(describing: response))")
                self.stopProgressLoading()
                failed(response as AnyObject!)
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("O/P responseString = \(String(describing: responseString))")
            
            self.stopProgressLoading()
            success(responseString as AnyObject!)
        }
        task.resume()
    }
    
    private func startProgressLoading(isStart: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        if (isStart == true) {
            DispatchQueue.main.async {
                let window = UIApplication.shared.keyWindow!
                _ = MBProgressHUD.showAdded(to: window, animated: true)
            }
        }
    }
    
    private func stopProgressLoading() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            MBProgressHUD.hide(for: UIApplication.shared.keyWindow!, animated: true)
        }
    }
    
}

