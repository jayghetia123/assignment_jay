//
//  Response.swift
//  Assignment
//
//  Created by JayMac on 27/10/17.
//  Copyright © 2017 JayMac. All rights reserved.
//

import Foundation
import UIKit

enum ResponseStatus: String, Codable {
    
    case False = "0"
    case Success = "1"
}

class Response: NSObject {

    var strMessage : String? = "Seems like your internet is not working. Please check your connection and try again."
    var intStatus : String?
    var isSuccess : ResponseStatus!
    var data : AnyObject?
    
    init(response :AnyObject) {
        
        super.init()
        
        print(response)
        let dictResponse = convertStringToDictionary(text : response as! String)
        if let responseData = dictResponse {
            self.data = responseData as AnyObject
            self.isSuccess = ResponseStatus(rawValue: "1")
        } else {
            self.isSuccess = ResponseStatus(rawValue: "0")
        }
        
    }
    
    func convertStringToDictionary(text: String) -> [Any]? {
        
        if let data = text.data(using: .utf8) {
            do {
                
                return try JSONSerialization.jsonObject(with: data, options: []) as? [Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

