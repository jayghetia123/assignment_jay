//
//  AppUtility.swift
//  Assignment
//
//  Created by JayMac on 27/10/17.
//  Copyright © 2017 JayMac. All rights reserved.
//

import UIKit
import Foundation


func getStringFromAnyObject(_ obj : Any?) -> String {
    
    if let value = obj as? String{
        return value
    }else{
        
        if let str = obj {
            return "\(String(describing: str))"
        } else {
            return ""
        }
    }
}

//Display alert in application
func showAlert(_ message:String) {
    
    let alert = UIAlertController(title: "Movies", message: message as String, preferredStyle: .alert)
    let defaultButton = UIAlertAction(title: "OK", style: .default) {(_) in
        
    }
    alert.addAction(defaultButton)
    UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true) {
        
    }
}
