//
//  AppConstants.swift
//  Assignment
//
//  Created by JayMac on 27/10/17.
//  Copyright © 2017 JayMac. All rights reserved.
//

import UIKit

let SCREEN_SIZE = UIScreen.main.bounds
let SCREEN_WIDTH = SCREEN_SIZE.width
let SCREEN_HEIGHT = SCREEN_SIZE.height

let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate

